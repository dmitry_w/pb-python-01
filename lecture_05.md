# Лекция 5

## Модули

Модуль в Python - это обычный текстовый файл с кодом Python и расширением .py. Он позволяет логически упорядочить и сгруппировать код.

### Импорт модуля

- import module
- import module as new_name
- from module import object
- from module import *

      import os

      os.listdir()
      os.

      os.cpu_count()
      os.listdir()
      os.environ
      os.cpu_count()
      os.environ

      import os as oska
      from os import os getlogin, getcwd

После имрорта модуль будет отображаться в текущем окружении.

Чтобы посмотреть доступные модули можно использовать компнду 
  
    dir()


### Создание своих модулей

chk_ip.py 

    import ipaddress

    def check_ip(ip):
        try:
            ipaddress.ip_address(ip)
            return True
        except ValueError as err:
            return False
    print(check_ip('1.1.1.1'))
    print(check_ip('1.1.1.333'))

Импортировать его можно так:

    import chk_ip

    from chk_ip import check_ip

    from chk_ip import *


### if __name__ == "__main__"

    import ipaddress

    def check_ip(ip):
        try:
            ipaddress.ip_address(ip)
            return True
        except ValueError as err:
            return False

    if __name__ == '__main__':
        print(check_ip('1.1.1.1'))
        print(check_ip('1.1.1.333'))


## Assert

assert ничего не делает, если выражение, которое написано после него истинное и генерирует исключение, если выражение ложное:


Примеры:

    assert 5 == 5
    assert 2 != 0
    assert 10 in [5, 10, 15, 20]
    assert '1999'.isdigit() == True
    assert 5 >= 0
    
### Вывод сообщения при ошибке

    assert sum([1, 1, 1]) == 6, "Should be 6"


Создадим файл с функцией проверки года и обложим ее тестами:

    def is_leap(y):
        """
        Високосный ли год?
        :param y: год
        :return: True - високосный, False - обычный
        """
        if (y % 100 == 0 and y % 400 == 0) or (y % 4 == 0 and y % 100 != 0):
            return True
        return False
    
    if __name__ == '__main__':
        assert is_leap(1900) == False
        assert is_leap(2016) == True
        assert is_leap(2019) == False
        assert is_leap(2020) == True

        leap_list = [2028, 2024, 2020, 2016, 2012, 
                     2008, 2004, 2000, 1996, 1992,
                     1988, 1984, 1980, 1976, 1972,
                     1968, 1964, 1960, 1956, 1952,
                     1948, 1944, 1940, 1936, 1932,
                     1928, 1924, 1920, 1916, 1912]
        for year in leap_list:
            assert is_leap(year) == True, 'Ошибочка!'

Игнорироапние assert в коде скрипта

    python -O script.py


## Основы ООП

    class User:
        uid = '56321485'
        first_name = 'Ivan'
        last_name = 'Zakharov'
        phone = '+79101234567'

Добавим метод:

    class User:
        uid = '56321485'
        first_name = 'Ivan'
        last_name = 'Zakharov'
        phone = '+79101234567'

        def show(self):
            print(f"""
            ID:      {User.uid}
            Имя:     {User.first_name}
            Фамиоия: {User.last_name}
            Телефон: {User.phone} 
            """)

Переделаем класс, чтобы он принимал данные (с ошибкой):

    class User:
        def __init__(self, uid, name, l_name, mobile):
          uid = uid
          first_name = name
          last_name = l_name
          phone = mobile

        def show(self):
            print(f"""
            ID:      {User.uid}
            Имя:     {User.first_name}
            Фамиоия: {User.last_name}
            Телефон: {User.phone} 
            """)

И создадим экземляр класса:

    user = User('56321485', 'Ivan', 'Zakharov', '+79101234567')

Теперь сделаем как надо:

    class User:
        def __init__(self, uid, name, l_name, mobile):
          self.uid = uid
          self.first_name = name
          self.last_name = l_name
          self.phone = mobile

        def show(self):
            print(f"""
            ID:      {self.uid}
            Имя:     {self.first_name}
            Фамиоия: {self.last_name}
            Телефон: {self.phone} 
            """)

И создаем экземпляр:

    user = User('56321485', 'Ivan', 'Zakharov', '+79101234567')


## API

### Пример просмотра запроса в браузере

![](./picture/api_yaru.png)

Пример запроса в консоли через curl:

по дефолту выполняется GET запрос:

    curl 'http://ya.ru'
    curl -X GET 'http://ya.ru'

Чтобы посмотреть код ответа:

    curl -I -X GET 'http://ya.ru'

### Модуль requests

Для работы с API в python существует внешний модуль requests

#### Установка и использование

    pip install requests

    import requests
    requests.get('http://ya.ru')

#### методы

#### Пример простого скрипта

    import requests

    def get_yaru():
        response = requests.get('http://ya.ru')
        return response

    if __name__ == '__main__':
        assert get_yaru().ok == True
        assert get_yaru().encoding == 'UTF-8'
        assert get_yaru().status_code == 200
        assert 'Найти' in get_yaru().text

Запрос с параметрами:

Для начала посмотрим ответ без параметров:

    import requests
    url = 'https://wttr.in'
    r = requests.get(url)
    print(r.text)

Вывести погоду за один день:

    params = {'0': '',
              'T':'',
              'lang':'ru'}
    r = requests.get(url, params)
    print(r.text)

Вывести, только температуру:

    params = {'format':'1', 'M':''}
    r = requests.get(url, params)
    print(r.text)

## API + ООП

    import requests

    class GetWeather:
        def __init__(self):
            self.url = 'https://wttr.in'
            self.default_params = {'lang':'ru'}

        def three_days(self):
            r = requests.get(self.url, self.default_params)
            print(r.text)

        def today(self):
            params = {'0': '',
                      'T':''}
            params.update(self.default_params)
            r = requests.get(self.url, params)
            print(r.text)

        def temp(self):
            params = {'format':'1', 'M':''}
            r = requests.get(self.url, params)
            print(r.text)

    pogoda = GetWeather()

Если мы например хотим 'скрыть' переменные
- url
- default_params
то достаточно добавить "_" в начале имени переменной:

      import requests

      class GetWeather:
          def __init__(self):
              self._url = 'https://wttr.in'
              self._default_params = {'lang':'ru'}

          def three_days(self):
              r = requests.get(self._url, self._default_params)
              print(r.text)

          def today(self):
              params = {'0': '',
                        'T':''}
              params.update(self._default_params)
              r = requests.get(self._url, params)
              print(r.text)

          def temp(self):
              params = {'format':'1', 'M':''}
              r = requests.get(self._url, params)
              print(r.text)

Создаем экземпляр:

    pogoda = GetWeather()




Пишите хорошие классы!

![](./picture/your_oop.png)


## Дополнительные материалы по лекции 5

- [Assert](https://pythonz.net/references/named/assert/)
- 
- [unittest.mock](https://docs.python.org/3/library/unittest.mock.html)