# Лекция 03

## if / elif / else

Пример:

    a = 10

    if a == 10:
        print('a равно 10')
    elif a < 10:
        print('a меньше 10')
    else:
        print('a больше 10')


Конструкция if/elif/else позволяет делать ответвления в ходе программы. Программа уходит в ветку при выполнении определенного условия.

В конструкции if/elif/else только if является обязательным, elif и else опциональны:

* Проверка if всегда идет первой.
* После оператора if должно быть какое-то условие: если это условие выполняется (возвращает True), то действия в блоке if выполняются.
* С помощью elif можно сделать несколько разветвлений, то есть, проверять входящие данные на разные условия.
* Блок elif это тот же if, но только следующая проверка. Грубо говоря, это «а если …»
* Блоков elif может быть много.
* Блок else выполняется в том случае, если ни одно из условий if или elif не было истинным.



### Условия

Конструкция if построена на условиях: после if и elif всегда пишется условие. Блоки if/elif выполняются только когда условие возвращает True, поэтому первое с чем надо разобраться - это что является истинным, а что ложным в Python.

### True и False (булевы значения)

В Python, кроме очевидных значений True и False, всем остальным объектам также соответствует ложное или истинное значение:

* истинное значение:
  - любое ненулевое число
  - любая непустая строка
  - любой непустой объект

    ```
    if 5: # 5, 'abc', [1,2,3], (4,5,6)
        print('истина')
    else:
        print('ложь')
    ```

* ложное значение:
  - 0
  - None
  - пустая строка
  - пустой объект

    ```
    if 0: # '', [], {}
        print('истина')
    else:
        print('ложь')
    ```

### Операторы сравнения

    print(5 > 2)
    print(10 < 2)
    print(8 == 8)
    print(4 is 4)
    print(5 is not 5)
    print(5 != 3)
    print(5 == 1)


### Оператор in

Оператор in позволяет выполнять проверку на наличие элемента в последовательности (например, элемента в списке или подстроки в строке):

    'МИР' in 'Карта МИР'
    Out: True

    In : 'Сергей' in ['Олег', 'Петр', 'Иван']
    Out: False

    In : 'Олег' in ['Олег', 'Петр', 'Иван']
    Out: True


Пример проверки в словаре (по ключам):

    server = { 
            'location': 'Лубянка',
            'vendor': 'IBM',
            'model': 'i2570',
            'os': 'centos',
            'ip': '10.255.0.1'
    }

    In : 'os' in server
    Out: True

    In : 'centos' in server
    Out: False

### Операторы and, or, not

#### and

    print(True and True)
    print(True and False)
    print(False and True)
    print(False and False)



    server = {'location': 'Лубянка',
              'vendor': 'IBM',
              'model': 'i2570',
              'os': 'centos',
              'ip': '10.255.0.1'}
    clients = ['Олег', 'Петр', 'Иван']

    In : 'os' in server and 'Олег' in clients
    Out: True

#### or

    print(True or True)
    print(True or False)
    print(False or True)
    print(False or False)

#### not 

    In : 'Олег' not in ['Олег', 'Петр', 'Иван']
    Out: False

    In : True and not True
    Out: False




### Пример использования конструкции if/elif/else

    username = input('Введите имя пользователя: ')
    password = input('Введите пароль: ')

    if len(password) < 8:
        print('Пароль слишком короткий')
    elif username in password:
        print('Пароль содержит имя пользователя')
    else:
        print(f'Пароль для пользователя {username} установленю.')

### Разница if и elif на примере:

    a = 10
    if a > 2:
        print("a > 2")
    elif a > 5:
        print("a > 5")
    elif a > 8:
        print("a > 8")


    a = 10
    if a > 2:
        print("a > 2")
    if a > 5:
        print("a > 5")
    if a > 8:
        print("a > 8")

### Вариант записи в одну строку (Тернарное выражение)

    s = [1, 2, 3, 4]
    result = True if len(s) > 5 else False
    print(result)

## Цикл for

Пример простого цикло for:

    words = ['list', 'dict', 'tuple']
    upper_words = []

    for word in words:
        upper_words.append(word.upper())

    print(upper_words)


Пример работы цикла со словарем:

    server = {'location': 'Лубянка',
              'vendor': 'IBM',
              'model': 'i2570',
              'os': 'centos',
              'ip': '10.255.0.1'}
    for i in server:
        print(i)

В предыдущем примере цикл проходит только по ключам
Если нужно получать пары ключ > значение, используем метод .items()

    server = {'location': 'Лубянка',
              'vendor': 'IBM',
              'model': 'i2570',
              'os': 'centos',
              'ip': '10.255.0.1'}
    
    for key, value in server.items():
        print(key + ' > ' + value)


Пример цикла for с функцией range():

    for i in range(10):
        print(f'user_{i}')


### Вложенные for:

    deposits = ['зимний', 
                'летний', 
                'домашний']
    names = ['Иван','Федор',
             'Анна','Петр']

    for name in names:
        print(f'{name}, мы рады приветствовать вас')
        print('Пожалуйста выберите вклад из предложенных:')
        for deposit in deposits:
            print(f'    Вклад {deposit}')

### Совмещение for и if

    access_template = ['switchport mode access',
                       'switchport access vlan',
                       'spanning-tree portfast',
                       'spanning-tree bpduguard enable']

    fast_int = {'access': { '0/12':10,
                            '0/14':11,
                            '0/16':17,
                            '0/17':150}}

    for intf, vlan in fast_int['access'].items():
        print('interface FastEthernet' + intf)
        for command in access_template:
            if command.endswith('access vlan'):
                print(' {} {}'.format(command, vlan))
            else:
                print(' {}'.format(command))

Комментарии к коду:

- В первом цикле for перебираются ключи и значения во вложенном словаре fast_int[„access“]
- Текущий ключ, на данный момент цикла, хранится в переменной intf
- Текущее значение, на данный момент цикла, хранится в переменной vlan
- Выводится строка interface FastEthernet с добавлением к ней номера интерфейса
- Во втором цикле for перебираются команды из списка access_template
- Так как к команде switchport access vlan надо добавить номер VLAN:
  - внутри второго цикла for проверяются команды
  - если команда заканчивается на access vlan
    -выводится команда, и к ней добавляется номер VLAN
  - во всех остальных случаях просто выводится команда



## Цикл while

В цикле while, как и в выражении if, надо писать условие. Если условие истинно, выполняются действия внутри блока while. При этом, в отличие от if, после выполнения кода в блоке, while возвращается в начало цикла.

При использовании циклов while необходимо обращать внимание на то, будет ли достигнуто такое состояние, при котором условие цикла будет ложным.

    a = 5
    while a > 0:
        print(a)
        a -= 1 # Эта запись равнозначна a = a - 1
        print(f'"a" после уменьшения на единицу: {a}')

Пример запроса пароля через цикл while:

    username = input('Введите имя пользователя: ' )
    password = input('Введите пароль: ' )

    password_correct = False

    while not password_correct:
        if len(password) < 8:
            print('Пароль слишком короткий\n')
            password = input('Введите пароль еще раз: ' )
        elif username in password:
            print('Пароль содержит имя пользователя\n')
            password = input('Введите пароль еще раз: ' )
        else:
            print(f'Пароль для пользователя {username} установлен')
            password_correct = True


## break, continue, pass

В Python есть несколько операторов, которые позволяют менять поведение циклов по умолчанию.

### break

Оператор break позволяет досрочно прервать цикл:

пример с циклом for:

    for num in range(10):
        if num < 7:
            print(num)
        else:
            break

пример с циклом while

    i = 0
    while i < 10:
        if i == 5:
            break
        else:
            print(i)
            i += 1

пример break в цикле запроса пароля 

    username = input('Введите имя пользователя: ' )
    password = input('Введите пароль: ' )

    while True:
        if len(password) < 8:
            print('Пароль слишком короткий\n')
        elif username in password:
            print('Пароль содержит имя пользователя\n')
        else:
            print('Пароль для пользователя {} установлен'.format(username))
            # завершает цикл while
            break
        password = input('Введите пароль еще раз: ')

### Оператор continue

Оператор continue возвращает управление в начало цикла. То есть, continue позволяет «перепрыгнуть» оставшиеся выражения в цикле и перейти к следующей итерации.

пример с for:

    for num in range(5):
        if num == 3:
            continue
        else:
            print(num)

пример с while:

    i = 0
    while i < 6:
        i += 1
        if i == 3:
            print("Пропускаем 3")
            continue
            print("Это никто не увидит")
        else:
            print("Текущее значение: ", i)

пример с запросом пароля:

    username = input('Введите имя пользователя: ')
    password = input('Введите пароль: ')
    password_correct = False

    while not password_correct:
        if len(password) < 8:
            print('Пароль слишком короткий\n')
        elif username in password:
            print('Пароль содержит имя пользователя\n')
        else:
            print('Пароль для пользователя {} установлен'.format(username))
            password_correct = True
            continue
        password = input('Введите пароль еще раз: ')

### Оператор pass

Оператор pass ничего не делает. Фактически, это такая заглушка для объектов.

    for num in range(5):
        if num < 3:
            pass
        else:
            print(num)


## for/else while/else

В циклах for и while опционально может использоваться блок else.

- блок else выполняется в том случае, если цикл завершил итерацию списка
- но else не выполняется, если в цикле был выполнен break

пример с for/else:

    for num in range(5):
        print(num)
    else:
        print("Числа закончились")

Пример с while/else:

    i = 0
    while i < 5:
        if i == 3:
            break
        else:
            print(i)
            i += 1
    else:
        print("Конец")




## Работа с исключениями try/except/else/finally

### try/except

    try:
        2/0
    except ZeroDivisionError:
        print("You can't divide by zero")

Конструкция try работает таким образом:

- сначала выполняются блок try
- если ошибок не возникло, блок except пропускается, и выполняется дальнейший код
- в блоке try возникло исключение, оставшаяся часть блока try пропускается
    - если в блоке except указано исключение, которое возникло, выполняется код в блоке except
    - если исключение, которое возникло, не указано в блоке except, выполнение программы прерывается и выдается ошибка


Обратите внимание, что строка 'Cool!' в блоке try не выводится:

    try:
        print("Let's divide some numbers")
        2/0
        print('Cool!')
    except ZeroDivisionError:
        print("You can't divide by zero")

Возможна конструкция с несколькими except

Например, следующий скрипт делит два числа введенных пользователем:

    try:
        a = input("Введите первое число: ")
        b = input("Введите второе число: ")
        print("Результат: ", int(a)/int(b))
    except ValueError:
        print("Пожалуйста, вводите только числа")
    except ZeroDivisionError:
        print("На ноль делить нельзя")

Если нет необходимости выводить различные сообщения на ошибки ValueError и ZeroDivisionError, можно сделать так (файл divide_ver2.py)

    try:
        a = input("Введите первое число: ")
        b = input("Введите второе число: ")
        print("Результат: ", int(a)/int(b))
    except (ValueError, ZeroDivisionError):
        print("Что-то пошло не так...")

### try/except/else

В конструкции try/except, else выполняется если не было исключения.

    try:
        a = input("Введите первое число: ")
        b = input("Введите второе число: ")
        result = int(a)/int(b)
    except (ValueError, ZeroDivisionError):
        print("Что-то пошло не так...")
    else:
        print("Результат в квадрате: ", result**2)

### try/except/finally

Блок finally - это еще один опциональный блок в конструкции try. Он выполняется всегда, независимо от того, было ли исключение или нет.

    try:
        a = input("Введите первое число: ")
        b = input("Введите второе число: ")
        result = int(a)/int(b)
    except (ValueError, ZeroDivisionError):
        print("Что-то пошло не так...")
    else:
        print("Результат в квадрате: ", result**2)
    finally:
        print("Вот и сказочке конец, а кто слушал - молодец.")

### Когда использовать исключения

Как правило, один и тот же код можно написать и с использованием исключений, и без них.

Можно переписать таким образом без try/except:

    while True:
        a = input("Введите число: ")
        b = input("Введите второе число: ")
        if a.isdigit() and b.isdigit():
            if int(b) == 0:
                print("На ноль делить нельзя")
            else:
                print(int(a)/int(b))
                break
        else:
            print("Поддерживаются только числа")

Но далеко не всегда аналогичный вариант без использования исключений будет простым и понятным.

Важно в каждой конкретной ситуации оценивать, какой вариант кода более понятный, компактный и универсальный - с исключениями или без.

Если вы раньше использовали какой-то другой язык программирования, есть вероятность, что в нём использование исключений считалось плохим тоном. В Python это не так. Чтобы немного больше разобраться с этим вопросом, посмотрите ссылки на дополнительные материалы в конце этого раздела.


## Дополнительные материалы по лекции 3

* [Условные конструкции в Python](http://pythonicway.com/python-conditionals)
* [Циклы в Python](http://pythonicway.com/python-loops)
* [Обработка исключительных ситуаций в Python](http://pythonicway.com/python-exceptions-handling)
* [Python Exception Handling Techniques](https://doughellmann.com/blog/2009/06/19/python-exception-handling-techniques/)
[Built-in Exceptions](https://docs.python.org/3/library/exceptions.html#built-in-exceptions)