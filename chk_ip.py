import ipaddress

def check_ip(ip : str):
    try:
        ipaddress.ip_address(ip)
        return True
    except ValueError as err:
        return False

print(check_ip('1.1.1.1'))
print(check_ip('1.1.1.333'))


