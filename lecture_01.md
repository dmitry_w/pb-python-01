# Лекция 1

## Вводная часть (README.md)


## Настраивание среды

* Установка ПО
  * python  https://www.python.org/downloads/windows/
  * git     https://git-scm.com/downloads 
  * PyCharm   https://www.jetbrains.com/ru-ru/pycharm/



## Запуск и работа

* python
  * запуск в консоли ОС (cmd / powershell)

* PyCharm
  * Запуск (создание проекта)
  * Выбор интерпретатора
  * Интерфейс
    - Дерево директорий
    - Создание файла
    - Окно редактора
    - Структура файла
    - Terminal / Console
    - Пишем Hello world

## Основы Python:

* [Python Дзен](https://tyapk.ru/blog/post/the-zen-of-python)

        import this

### Что такое Pythob

* Объектно ориентированный язык с динамической типизацией (функция type)

        a = 5
        print(a)
        print(type(a))

        a = 'Hello'
        print(a)
        print(type(a))

  * Плюсы
    * Скорость написания кода (не нужно объявлять переменные)
  * Минусы
    - Может вызвать путаницу (в какой момент какии типом данных является тот или иной объект)


### Типы данных в Python

* Изменяемые и не изменяемые

#### [Числа](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/3_numbers.html)

* Числа int / float
    
        print(2)
        print(12.0124)

    * Операторы сложения вычетания умножения деления

            print(2+2)
            print(5-1)
            print(15*15)
            print(10/2.5)
            print(10/3)

    * Остаток от деления %

            print(5%2)      >> 1
            print(4%2)      >> 0

    * Целочисленное деление //

            print(5//2)     >> 2
            print(10//3)    >> 3

* Полезные функции
    * round
        
            print(round(10/3.0, 2))     >> 3.33

    * int / float / str 

            print(int('125') + 25)
            print(float('1.5') + 1.5)
            print('Hello number' + str(47))

    Дополнительные материалы:
    * [Операторы в Python](http://pythonicway.com/python-operators)
    * [Числа: целые, вещественные, комплексные](https://pythonworld.ru/tipy-dannyx-v-python/chisla-int-float-complex.html)


#### [Строки](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/4_strings.html)

        print('Hello world!')
        print('Кнопка "старт" на панели')
        print("Магазин 'пятерочка' дает 'много' бонусов")

* Действия
    * Суммирование/Умножение (Конкатенация)

            print('#' * 20)
            print('#### ' + 'Hello' + ' ' + 'world' + '!')
            print('#' * 20)


    * Обращение к элементам строки по индексу

            print('Hello world !'[1])
            print('Hello world !'[-3])

    * Срезы from:to:step

            print('Hello world, im so happy to talk with you !'[6:])
            print('Hello world, im so happy to talk with you !'[:15])
            print('Hello world, im so happy to talk with you !'[0:4])
            print('hohohohahaha'[::2])
            print('Hello world, im so happy to talk with you !'[-1::-1])

* Полезные функции применимые к строкам
    * len

            print(len('Hello world, im so happy to talk with you !'))

    * int / float / bytes

            print(int('125') + 25)
            print(float('1.5') + 1.4)

            print(bytes('Привет!', encoding="utf_8"))
            print(bytes('Привет!', encoding="utf_8").decode("utf-8"))

    Дополнительные материалы:
    * [Работа со строками в Python: литералы](https://pythonworld.ru/tipy-dannyx-v-python/stroki-literaly-strok.html)
    * [Байты (bytes и bytearray)](https://pythonworld.ru/tipy-dannyx-v-python/bajty-bytes-i-bytearray.html)


##### [Полезные методы строк](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/4a_string_methods.html)

  * upper, lower, swapcase, capitalize

        print('вася'.upper())
        print('ВАСЯ'.lower())
        print('Вася'.swapcase())
        print('вася'.capitalize())

  * count кол-во вхождений

        print('Hello world'.count('l'))

  * find / rfind первое вхождение

        print('Hello world'.find('l'))
        print('Hello world'.rfind('l'))

  * replace

        print('Hello world'.replace('world', 'my friend'))

  * strip (по умолчанию убирает \t\n\r\f\v) lstrip/rstrip (в начале или в конце или и там и там)
    
        "\nHello \nworld.\n \nIt's \nwonderful \nworld\n"

  * split (по умолчанию несколько пробелов метод считает за один)

        print('Hello    my dear friend   !'.split())
        print('Hello_my_dear_friend_!'.split('_'))

    Дополнительные материалы:
    * [Строки. Функции и методы строк](https://pythonworld.ru/tipy-dannyx-v-python/stroki-funkcii-i-metody-strok.html)
    

##### [Форматирование строк](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/4b_string_format.html)

  * передача переменных в print

        name = 'Dima'
        age = '32'
        print('Hello, my name is ', name, 'Im ', age, 'old.')

  * %

        print('Hello %s!' % 'Vova')
        print('Hello %s! My name is %s' % ('Vova', 'Dima'))

  * format {:>15} {:15} {:08b}

        print('Hello {}!'.format('Vova'))
        print('Hello {}! My name is {}'.format('Vova', 'Dima'))
        print('Hello {0}! My name is {1}. So, how are you {0}?'.format('Vova', 'Dima'))
        print('Hello {you}! My name is {me}. So, how are you {you}?'.format(you='Vova', me='Dima'))
        
  * f-строки

        you = 'Vova'
        me = 'Dima'
        print(f'Hello {you}! My name is {me}')

        
Дополнительные материалы:
* [None или немного о типе NoneType](https://pythonworld.ru/tipy-dannyx-v-python/none.html)
* [Основы синтаксиса Python](http://pythonicway.com/python-basic-syntax)

### Домашнее задание

