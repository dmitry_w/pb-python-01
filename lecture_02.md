# Лекция 2

## [Список / List](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/5_lists.html)

Последовательность элементов разделенные запятой.

    list1 = [10,20,30,77]
    list2 = ['one', 'dog', 'seven']
    list3 = [1, 20, 4.0, 'word']

### Создание списка

- С помощью литерала (Литерал - это выражение, которое создает объект.)

      nums = [10, 20, 30, 50]

- С помощью функции list()

      list1 = list('client')
      print(list1)

### Обращение к элементам списка

    list3 = [1, 20, 4.0, 'word']

    print(list3[1::])
    print(list3[-1])
    print(list3[::-1])


### Изменение элементов списка

    list3 = [1, 20, 4.0, 'word']
    print("Before : ", list3)
    list3[0] = 'test'
    print("After : ", list3)

### Список в списке

    client = [['Иван', 'Овечкин', 'Visa', '*5611', '10/24'],
              ['Федор', 'Иванов', 'Мир', '*6415', '08/26'],
              ['Сергей', 'Васильев', 'MC', '*8102', '10/20']]

    print(client[1][0:2])
    print(client[2][-1])
    print(client[0])

### Функции

- len()

      print(len(client))

- sorted()
  Возвращает новый отсортированный список (не меняет исходный)
    
      names = ['John', 'Michael', 'Antony']
      print(sorted(names))
      print(sorted(names, reverse=True))

### [Методы списков](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/5a_list_methods.html)

#### .reverse()
  Меняет последовательность элементов (не возвращает результат)

      nums = ['10', '15', '20', '30', '100-200']
      nums.reverse()
      print(nums)


#### .join()

Как аргумент ожидает список строк и возвращает строку в качестве разделителя выступает строка, к которой применен метод join()

      nums = ['10', '20', '30']
      print("_".join(nums))
      print(type("_".join(nums)))

#### .append()

Добавляет передаваемый аргумент в конец списка

      nums = ['10', '20', '30']
      nums.append('40')
      print(nums)

#### .extend()

Объеденяет два списка (изменяет список к которому применен метод)

      num1 = ['10', '20', '30']
      num2 = ['40', '50', '60']
      num1.extend(num2)
      print(num1)

  Тогда как сложение списов - возвращает новый список

      print(num1 + num2)
      print(num1)
      print(num2)

#### .pop()

Возвращает убирает из списка элемент по индексу и возвращает его (-1 по умолчанию)

      nums = ['10', '20', '30', '40', '50', '60']
      print(nums.pop())
      print(nums)

      print(nums.pop(0))
      print(nums)

#### .remove()

Он тоже как и .pop() удаляет указанный элемент по содержимому, но в отличии от pop - не возвращает удаляемый элемент

      nums = ['10', '20', '30', '40', '50']
      print(nums.remove('30'))
      print(nums)

#### .index()

Ищет по значению индекс элемента и возвращает индекс первого вхождения с лева на право

      nums = ['10', '20', '30', '40', '20']
      print(nums.index('20'))

#### .insert()

Встявляет передаваемый элемент в указанную позицию

      nums = ['10', '20', '30', '40', '50']
      nums.insert(2, '25')
      print(nums)

#### .sort()

Сортирует список к которому применяется метод

      nums = [1, 50, 10, 15]
      nums.sort()
      print(nums)



## [Словарь / Dict](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/6_dicts.html])

Словари - это изменяемый упорядоченный тип данных:
- данные в словаре - это пары ключ: значение
- доступ к значениям осуществляется по ключу, а не по номеру, как в списках
- данные в словаре упорядочены по порядку добавления элементов
- так как словари изменяемы, то элементы словаря можно менять, добавлять, удалять
- ключ должен быть объектом неизменяемого типа: число, строка, кортеж
значение может быть данными любого типа

Примеры:

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}

    users = {
        'name': 'Петр',
        'city': 'moscow',
        'active': True
    }

Получение значения из словаря:

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    print(users['city'])
  
Добавление нового ключ+значение

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    users['credit_type'] = 'super_credit'
    print(users)

В качастве значения может использоваться словарь:

    users = {
        'name': 'Петр',
        'city': 'moscow',
        'active': True,
        'cards': {
              'visa': True,
              'Мир': False,
              'МС': True 
        }
    }

Получать значение из вложенного словаря можно так:

    users['cards']['visa']

Функция sorted сортирует ключи словаря по возрастанию и возвращает новый список с отсортированными ключами:

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    list_of_sorted_keys = sorted(users)
    print(list_of_sorted_keys)


### [Методы словаря](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/6a_dict_methods.html)

#### .clear()

полностью очищает словарь

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    users.clear()
    print(users)
    print(type(users))

#### .copy()

Создает копию словаря

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    users2 = users.copy()
    print(users2
    
В случае присваивания - мы получим ссылку на тот же самы словарь

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    users2 = users
    print(id(users))
    print(id(users2))

#### .get()

Если при обращении к словарю указывается ключ, которого нет в словаре, возникает ошибка:

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    print(users['age'])

Метод get() запрашивает ключ, и если его нет, вместо ошибки возвращает None

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    print(users.get('age'))

Метод get() позволяет также указывать другое значение вместо None:

    print(users.get('age', 'Ooops!'))

#### .setdefault()

Метод setdefault() ищет ключ, и если его нет, вместо ошибки создает ключ со значением

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    print(users.setdefault('age'))
    print(users)

этот метод заменяет подобную конструкцию:

    if key in users:
              value = users[key]
    else:
              users[key] = 'somevalue'
              value = users[key]

#### .keys(), .values(), .items()

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}

    keys = users.keys()
    values = users.values()
    items = users.items()

    print(keys)
    print(values)
    print(items)

### del (функция)

Удаляет ключ и значение

    users = {'name': 'Петр', 'city': 'moscow', 'active': True, 'age'= 35}
    del(users['age'])
    print(users)

#### .update()

Метод update позволяет добавлять в словарь содержимое другого словаря (или обновлять, если ключ уже есть):

    users = {'name': 'Петр', 'city': 'moscow', 'active': True}
    db = {'id': '1wqd334te'}
    users.update(db)
    print(users)

## [Варианты создания словаря](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/6b_create_dict.html#)

### dict

    users = dict(name = 'Петр', city='moscow', active=True)
    print(users)

Вриант передать список кортежей

    users = dict([('name','Петр'), ('city','moscow')])
    print(users)

### dict.fromkeys

    d_keys = ['name', 'city', 'active']
    users = dict.fromkeys(d_keys)
    print(users)




## [Кортеж / Tuple](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/7_tuple.html)

Кортеж в Python это:
- последовательность элементов, которые разделены между собой запятой и заключены в скобки
- неизменяемый упорядоченный тип данных


### Примеры кортежа:

Создать пустой кортеж:

    tuple1 = tuple()
    print(tuple1)

Кортеж из одного элемента (обратите внимание на запятую):

    tuple2 = ('password',)
    print(tuple2)
    print(type(tuple2))

Кортеж из списка:

    list_nums = ['10', '20', '30', '40', '50']
    tuple_nums = tuple(list_nums)
    print(tuple_nums)

### Обращение к элементам кортежа

    tuple3 = (1, 20, 4.0, 'word')

    print(tuple3[1::])
    print(tuple3[-1])
    print(tuple3[::-1])

### Функции

К кортежу можно применить функцию sorted, но на выходе получим отсортированный список

    my_tuple = (60, 10, 80, 20, 40)
    print(sorted(my_tuple))


## [Множества / Set](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/8_set.html)

Множество - это изменяемый неупорядоченный тип данных. В множестве всегда содержатся только уникальные элементы.

    set1 = {10, 20, 30, 40, 10, 20, 100}
    print(set1)

    set2 = {'Оля', 'Миша', 'Маша', 'Аня', 'Оля', 'Аня'}
    print(set2)

### [Методы для множества](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/8a_set_methods.html)

#### .add()

    set5 = {'Аня', 'Маша', 'Миша', 'Оля'}
    set5.add('Юра')
    print(set5)

#### .discard()

Удалить элемент не выдавая ошибку при его отсутствии

    set = {'Аня', 'Маша', 'Миша', 'Оля'}
    set.discard('Аня')
    set.discard('Аня')
    print(set)

#### .clear()

Очищает множество

    set2 = {'Аня', 'Маша', 'Миша', 'Оля'}
    set2.clear()
    print(set2)


#### .union()

Объеденяет два множества (возвращает новое множество)

    set1 = {'Аня', 'Маша'}
    set2 = {'Миша', 'Оля'}
    set3 = set1.union(set2)
    print(set3)

Еще способ:

    set3 = set1 | set2
    print(set3)


## [Преобразования типов](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/9a_check_type.html)

### Строка в число

    In : int("10")
    Out: 10


### Преобразование в список (list)

    In : list("string")
    Out: ['s', 't', 'r', 'i', 'n', 'g']

    In : list({1,2,3})
    Out: [1, 2, 3]

    In : list((1,2,3,4))
    Out: [1, 2, 3, 4]


### Преобразование во множество (set)

    In : set([1,2,3,3,4,4,4,4])
    Out: {1, 2, 3, 4}

    In : set((1,2,3,3,4,4,4,4))
    Out: {1, 2, 3, 4}

    In : set("string string")
    Out: {' ', 'g', 'i', 'n', 'r', 's', 't'}


### Преобразование в кортеж (tuple)

    In : tuple([1,2,3,4])
    Out: (1, 2, 3, 4)

    In : tuple({1,2,3,4})
    Out: (1, 2, 3, 4)

    In : tuple("string")
    Out: ('s', 't', 'r', 'i', 'n', 'g')

### Преобразование в строку

    In : str(10)
    Out: '10'