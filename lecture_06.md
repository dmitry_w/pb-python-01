# Лeкция 6

## Тестирование web интерфейса

### Примкр магии

Демонстрация авторизации в интернет банк.

### XPath

XPath - Язык запросов к элементам XML-документа

Пример xml/html и xpath:

    <html>
        <body>
            <div>Первый слой
                <span>блок текста в первом слое</span>
            </div>
            <div>Второй слой</div>
            <div>Третий слой
                <span class="text">первый блок в третьем слое</span>
                <span class="text">второй блок в третьем слое</span>
                <span>третий блок в третьем слое</span>
            </div>
                <span>четвёртый слой</span>
            <img />
        </body>
    </html>

    //body/div[1]/span
    //body/div[3]/span[@class='text']

#### Синтаксис

Положение:

// - N-ое количество элементов
.. - родительский элемент

html элементы:

div, button, input, label, li, ol, td, tr, ...
[] - содержимое эдемента или порядковый номер элемента

Содержимое

text()
@class
@id
@name

Примеры:

    //button[text()='Да']
    //button[contains(text(), 'Перейти в ')]
    //label[contains(text(), 'Выбор сети')]/..//img[@qtip = 'Add']
    //input[@name='ephemeral']
    //button[not(@disabled) and @type='submit' and contains(.,'Далее')]

Использование шаблона xpath:

    xpath_template = "//button[contains(text(), '{}')]"
    xpath = xpath_template.format('Взять кредит наличными')


### Selenium

    URL = "https://mytest.pochtabank.ru:943/"
    login = 'u8918486'
    passwd = 'qwe1234'
    otp = '111111'

    from selenium import webdriver

    driver = webdriver.Chrome("./chromedriver")
    driver.get(URL)

#### Получение элемениа

    element = driver.find_element_by_xpath()
    element = driver.find_element_by_id()
    element = driver.find_element_by_name()
    element = driver.find_element_by_class_name()
    element = driver.find_element_by_tag_name()
    element = driver.find_element_by_css_selector()
    element = driver.find_element_by_link_text()


#### Методы для элемента

Проверка:

    element.is_enabled()
    element.is_displayed()
    element.is_selected()

По действию:

    element.clear()
    element.send_text('user11')
    element.click()

Получение данных:

    element.text
    element.get_attribute("class")
    element.tag_name
    element.get_attribute


#### Ожидание элемента

##### Явные ожидания

    from time import sleep
    sleep(5)

##### Правильные ожидания

    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC

    driver = webdriver.Chrome("./chromedriver")
    driver.get("http://somedomain/url_that_delays_loading")
    try:
        element = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "myDynamicElement"))
    )
    finally:
        driver.quit()


## Дополнительные материалы по лекции 6

* [Мануал по XPath](https://docs.google.com/document/d/1PdfKMDfoqFIlF4tN1jKrOf1iZ1rqESy2xVMIj3uuV3g/pub)
* [XPath in Selenium WebDriver: Complete Tutorial](https://www.guru99.com/xpath-selenium.html)
* [Chrome Driver](https://chromedriver.chromium.org/downloads)
* [Selenium для Python. Глава 3. Навигация](https://habr.com/ru/post/250947/)
* [Selenium для Python. Глава 5. Ожидания](https://habr.com/ru/post/273089/)
* [Selenium with Python](https://selenium-python.readthedocs.io/)
* [XPath Tester](https://www.freeformatter.com/xpath-tester.html#ad-output)
