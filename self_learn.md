# Материалы для самообучения

# Курс 

* [Python для сетевых инженеров](https://pyneng.readthedocs.io/ru/latest/)Книга на которой основан текущий курс
* [Плейлист на youtube с записью вебинаров](https://www.youtube.com/playlist?list=PLah0HUih_ZRnJFNdZsWr2pNWgYETauGXo
)
* [Примеры и задания](https://github.com/natenka/pyneng-examples-exercises)


## Git

* [Pro Git](https://git-scm.com/book/ru/v2) Подробнейшее руководство по Git
* [Learn git branching](https://learngitbranching.js.org/) Интерактивный тренажер для хорошего и наглядного понимания работы Git


## Python

### Книги





### Web интерактив

* [Яндекс практикум](https://praktikum.yandex.ru/profile/backend-developer/) Интерактивный курс для изучения основ python (вводный блок - бесплатный)
* [Code Wars](https://www.codewars.com/) Возможность попроблвать свои силы задачках различных сложностей и сравнить свои решения с другими участниками





## Python тесты для самопроверки

* [Типы данных. Часть 1](https://goo.gl/forms/xKHX5xNM8Pv5sQDf2)
* [Типы данных. Часть 2](https://goo.gl/forms/igxR3ub3tQg3ycX53)
* [Контроль хода программы. Часть 1](https://goo.gl/forms/2TmGcrhG11h2SdLn1)
* [Контроль хода программы. Часть 2](https://goo.gl/forms/KZGaDquGlUmOz2kG3)
* [Функции и модули. Часть 1](https://goo.gl/forms/M1DpbdD0brVbdp1G3)
* [Функции и модули. Часть 2](https://goo.gl/forms/rNvdX9bHw8wLajJp2)
* [Регулярные выражения. Часть 1](https://goo.gl/forms/5UpkJbm1dORqs4bP2)
* [Регулярные выражения. Часть 2](https://goo.gl/forms/ltuOAO62yLlZkEmm1)
* [Базы данных](https://goo.gl/forms/wtGgmWg0vow1Cyqo1)




## Дополнительные материалы по лекции 1

* [Python Дзен](https://tyapk.ru/blog/post/the-zen-of-python)
* [Числа](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/3_numbers.html)
* [Операторы в Python](http://pythonicway.com/python-operators)
* [Числа: целые, вещественные, комплексные](https://pythonworld.ru/tipy-dannyx-v-python/chisla-int-float-complex.html)
* [Строки](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/4_strings.html)
* [Работа со строками в Python: литералы](https://pythonworld.ru/tipy-dannyx-v-python/stroki-literaly-strok.html)
* [Байты (bytes и bytearray)](https://pythonworld.ru/tipy-dannyx-v-python/bajty-bytes-i-bytearray.html)
* [Полезные методы строк](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/4a_string_methods.html)
* [Строки. Функции и методы строк](https://pythonworld.ru/tipy-dannyx-v-python/stroki-funkcii-i-metody-strok.html)
* [Форматирование строк](https://pyneng.readthedocs.io/ru/latest/book/04_data_structures/4b_string_format.html)
* [None или немного о типе NoneType](https://pythonworld.ru/tipy-dannyx-v-python/none.html)
* [Основы синтаксиса Python](http://pythonicway.com/python-basic-syntax)




## Дополнительные материалы по лекции 3

* [Условные конструкции в Python](http://pythonicway.com/python-conditionals)
* [Циклы в Python](http://pythonicway.com/python-loops)
* [Обработка исключительных ситуаций в Python](http://pythonicway.com/python-exceptions-handling)
* [Python Exception Handling Techniques](https://doughellmann.com/blog/2009/06/19/python-exception-handling-techniques/)
[Built-in Exceptions](https://docs.python.org/3/library/exceptions.html#built-in-exceptions)
