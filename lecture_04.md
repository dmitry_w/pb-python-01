# Лекция 4

## List, dict, set comprehensions

### List comprehensions

Обычный цикл:

    items = ['10', '20', 'a', '30', 'b', '40']
    only_digits = []
    for item in items:
        if item.isdigit():
            only_digits.append(int(item))

List comprehensions:

    items = ['10', '20', 'a', '30', 'b', '40']
    only_digits = [int(item) for item in items if item.isdigit()]

Одновременный проход по двум последовательностям:

    vlans = [100, 110, 150, 200]
    names = ['cat', 'dog', 'bat', 'lion']

    result = ['number {} and name {}'.format(vlan, name) for vlan, name in zip(vlans, names)]
    print(result)

### Dict comprehensions

см. доп материалы в конце лекции


## Распаковка переменных

    client = ['Иван', 'Федоров', '25', '150000']
    first_name, last_name, age, balance = client
    print(f"""
    Иия:     {first_name}
    Фамилия: {last_name}
    Возраст: {age}
    Баланс:  {balance}""")

### Использование *

Так можно получить первый элемент в переменную name, а все остальное в переменную other:

    client = ['Иван', 'Федоров', '25', '150000']
    name, *other = client
    print(f'name: {name}')
    print(f'other: {other}')

даже если элемент один - распаковка всеравно сработает:

    client = ['Иван']
    name, *other = client
    print(f'name: {name}')
    print(f'other: {other}')

### Распаковка zip

    a = [1,2,3,4,5]
    b = [100,200,300,400,500]
    z = zip(a, b)
    print([i for i in z])

### Распаковка словарей

    server = {'location': 'Лубянка',
            'vendor': 'IBM',
            'model': 'i2570',
            'os': 'centos',
            'ip': '10.255.0.1'}

    srv_sting = """
    У нас есть сервер.
    Он находится там, где {location}.
    Его модель {model}.
    Его IP адрес {ip}.
    В качестве базовой ОС используется {os}
    """
    print(srv_sting.format(**server))


## Работа с файлами

В работе с файлами есть несколько аспектов:
- открытие/закрытие
- чтение
- запись

### Открытие файла

Для начала работы с файлом, его надо открыть.


    file = open('file_name.txt', 'r')

Режимы открытия файлов:

- r - открыть файл только для чтения (значение по умолчанию)
- r+ - открыть файл для чтения и записи
- w - открыть файл для записи
  - если файл существует, то его содержимое удаляется
  - если файл не существует, то создается новый
- w+ - открыть файл для чтения и записи
    - если файл существует, то его содержимое удаляется
    - если файл не существует, то создается новый
- a - открыть файл для дополнения записи. Данные добавляются в конец файла
- a+ - открыть файл для чтения и записи. Данные добавляются в конец файла

Сокращения:

r - read; a - append; w - write

### Чтение файлов

- read() - считывает содержимое файла в строку
- readline() - считывает файл построчно
- readlines() - считывает строки файла и создает список из строк

  f = open('readfile.txt', 'r')

  f.read()
  f.readline()
  f.readlines()

Но чаще всего проще пройтись по объекту file в цикле, не используя методы read...:

    f = open('readfile.txt')
    for line in f:
        print(line)

Еще вариант создания списка из файла (убрав символ переноса строка '\n'):

    f = open('readfile.txt')
    f.read().split('\n')

Метод seek()

Переносит курсор на заданную стоку

    f = open('readfile.txt')
    f.read()
    f.read()
    f.seek(0)
    f.read()


### Запись файлов

#### метод write

    f = open('write_file.txt', 'w')

    f.write('первая запись в файл')
    f.close()

#### writelines()

метод ожидает список строк

### Закрытие файлов

После завершения работы с файлом, его нужно закрыть. В некоторых случаях Python может самостоятельно закрыть файл. Но лучше на это не рассчитывать и закрывать файл явно.

#### close()

#### try/finally

    try:
        f = open('readfile.txt', 'r')
    except IOError:
        print('No such file')
    finally:
        f.close()

### Конструкция with

    with open('readfile.txt', 'r') as f:
        for line in f:
            print(line)

открытие двух файлов

    with open('readfile.txt') as src, open('result.txt', 'w') as dest:
        for line in src:
            if line.startswith('service'):
                dest.write(line)


## Функции

    def sum_list(lst):
        """
        Функция суммирует все элементы списка
        lst - список целых чисел
        """
        result = 0
        for i in lst:
            result += i
        return result

### Создание функций

* Функция определяется с зарезервированного слова def
* имя_функции(аргумент1, агрементN):
* """"докстринг"""
* код функции
* return завершает выполнение функции

* запуск функции(вызов)

        function()

### Пространства имен. Области видимости

У Python есть правило LEGB, которым он пользуется при поиске переменных

* L (local) - в локальной (внутри функции)
* E (enclosing) - в локальной области объемлющих функций (это те функции, внутри которых находится наша функция)
* G (global) - в глобальной (в скрипте)
* B (built-in) - во встроенной (зарезервированные значения Python)

- локальные переменные:
  - переменные, которые определены внутри функции
  - эти переменные становятся недоступными после выхода из функции
- глобальные переменные:
  -переменные, которые определены вне функции
  - эти переменные „глобальны“ только в пределах модуля
например, чтобы они были доступны в другом модуле, их надо импортировать

пример локальной переменной

    def config_func(server, ip):
        func_str = f'сервер: {server} с ip: {ip}'
        return func_str

    config_func('qa', '10.10.1.100')
    print(func_str)

### Параметры и аргументы функций

#### Обязательные параметры (позиционные агрументы)

    def check_passwd(username, password):
        if len(password) < 8:
            print('Пароль слишком короткий')
            return False
        elif username in password:
            print('Пароль содержит имя пользователя')
            return False
        else:
            print(f'Пароль для пользователя {username} прошел все проверки')
            return True

пример вывода позиционных аргументов

    a = 1
    b = 2
    def show(a, b):
        print(f'a = {a}')
        print(f'b = {b}')

    show(a, b)


#### Необязательные параметры (параметры со значением по умолчанию)

    def check_passwd(username, password, min_length=8):
        if len(password) < min_length:
            print('Пароль слишком короткий')
            return False
        elif username in password:
            print('Пароль содержит имя пользователя')
            return False
        else:
            print(f'Пароль для пользователя {username} прошел все проверки')
            return True

пример вывода ключевых аргументов

    def show(b=2, a=1):
        print(f'a = {a}')
        print(f'b = {b}')
        
    show()

### Аргументы переменной длины

    def sum_arg(a, *args):
        print(a, args)
        return a + sum(args)

    sum_arg(1,2,3,4)

### Распаковка аргументов

Допустим есть функция по проверке пароля

    def check_passwd(username, password, min_length=8, check_username=True):
        if len(password) < min_length:
            print('Пароль слишком короткий')
            return False
        elif check_username and username in password:
            print('Пароль содержит имя пользователя')
            return False
        else:
            print(f'Пароль для пользователя {username} шел все проверки')
            return True

Сделаем функцию которая записывает пользователя и парольв файл:

    def add_user_to_users_file(user, users_filename='users.txt'):
        while True:
            passwd = input(f'Введите пароль для пользователя {user}: ')
            if check_passwd(user, passwd):
                break
        with open(users_filename, 'a') as f:
            f.write(f'{user},{passwd}\n')

В таком варианте функции add_user_to_users_file нет возможности регулировать минимальную длину пароля и то надо ли проверять наличие имени пользователя в пароле. В следующем варианте функции add_user_to_users_file эти возможности добавлены:

    def add_user_to_users_file(user, users_filename='users.txt', min_length=8, check_username=True):
        while True:
            passwd = input(f'Введите пароль для пользователя {user}: ')
            if check_passwd(user, passwd, min_length, check_username):
                break
        with open(users_filename, 'a') as f:
            f.write(f'{user},{passwd}\n')

Чтобы не дублировать аргументы можно использовать конструкцию **kwargs

    def add_user_to_users_file(user, users_filename='users.txt', **kwargs):
        while True:
            passwd = input(f'Введите пароль для пользователя {user}: ')
            if check_passwd(user, passwd, **kwargs):
                break
        with open(users_filename, 'a') as f:
            f.write(f'{user},{passwd}\n')

В таком варианте в функцию check_passwd можно добавлять аргументы без необходимости дублировать их в функции add_user_to_users_file.

### Рекурсия

    def recursion_sum(n):
        if n == 1: # условие выхода из рекурсии
            return 1
        return n + recursion_sum(n-1) # рекурсивный вызов

Распишем что поисходит при выполнении:

    recursion_sum(5)

    return 5 + recursion_sum(5-1)
            4 + recursion_sum(4-1)
                3 + recursion_sum(3-1)
                    2 + recursion_sum(2-1)
                            1


## Полезные функции

- print

    print(*items, sep=' ', end='\n', file=sys.stdout, flush=False)

- range

    range(start, stop[, step])

- start - с какого числа начинается последовательность. По умолчанию - 0
- stop - до какого числа продолжается последовательность чисел. Указанное число не включается в диапазон
- step - с каким шагом растут числа. По умолчанию 1


- sorted

    sorted(iterable, /, *, key=None, reverse=False)

С помощью параметра key можно указывать, как именно выполнять сортировку. Параметр key ожидает функцию, с помощью которой должно быть выполнено сравнение.

например отсортировать по длине:

    list_of_words = ['one', 'two', 'list', '', 'dict']
    sorted(list_of_words, key=len)

    Out: ['', 'one', 'two', 'list', 'dict']

Отсортировать игнорирую регистр:

- key=str.lower






## Дополнительные материалы по лекции 4

* [List, dict, set comprehensions](https://pyneng.readthedocs.io/ru/latest/book/08_python_basic_examples/x_comprehensions.html)
* [Распаковка переменных](https://pyneng.readthedocs.io/ru/latest/book/08_python_basic_examples/variable_unpacking.html)
* [Работа с файлами](https://pyneng.readthedocs.io/ru/latest/book/07_files/index.html)
* [Пространства имен. Области видимости](https://pyneng.readthedocs.io/ru/latest/book/09_functions/2_namespace.html)
* [Полезные функции](https://pyneng.readthedocs.io/ru/latest/book/10_useful_functions/index.html)